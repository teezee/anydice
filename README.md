# ANYDICE

    Copyright (c) 2021 thomas.zink _at_ uni-konstanz _dot_ de
    The works is free. Usage of the works is permitted under the terms of the 
    Do What The Fuck You Want To Public License. See LICENSE for details
    DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

library and scripts for handling several different dice rolls.

## Implemented Dice Mechanics

- Risk
- nDx: n dice of x sides
- exploding dice
- counting successes and success functions
- OpenD6
- Fate
- The End of the World

## Using the Telegram bot

- Create your bot and API Key using the `botfather`.
- Run the bot with yout API Key:
    ./eotw_telegram_bot.py <your-api-key>


## Requirements

Install requirements:

    pip3 install -r requirements.txt
