#!/bin/env python

import re
import random

def roll_dice(dice_code, target_number=None):
    """
    Roll the dice specified by the dice code and apply modifiers.

    Args:
        dice_code (str): The dice code to roll (e.g., "2d6+3", "1d20-2", "1d6+2d10", etc.)

    Returns:
        int: The result of the dice roll with modifiers applied.
    """
    
    # Split the input string into individual dice codes and modifiers
    parts = re.findall(r"([+-]?\d+d\d+[x]?)|([+-]\d+(?!d))", dice_code)

    # Initialize the total result
    total = 0

    # Initialize the sign
    sign = +1

    # Process each part
    for part in parts:
        # Check if the part is a dice code
        if part[0]:
            if part[0][0] == '-':
                sign = -1
            else:
                sign = +1
            # Extract the number of dice and sides from the match
            dice_match = re.match(r"[+-]?(\d*)d(\d+)(x)?", part[0])
            num_dice = int(dice_match.group(1)) if dice_match.group(1) else 1
            num_sides = int(dice_match.group(2))
            explode = dice_match.group(3)

            # Roll the dice
            rolls = [random.randint(1, num_sides) for _ in range(num_dice)]

            if explode:
                # explode rolls of x
                for roll in rolls:
                    if roll == num_sides:
                        rolls.append(random.randint(1, num_sides))

            # Add the result to the total
            total += sign * sum(rolls)

            # Print the individual rolls
            if sign == 1:
                sign_s = "+"
            else:
                sign_s = "-"

            print(f"{sign_s}{sum(rolls)} ({sign_s}{num_dice}d{num_sides} {rolls})")

        # Check if the part is a modifier
        elif part[1]:
            total += int(part[1])
            print(f"{part[1]}")
            
    # Print the total result
    print(f"= {total}")

    if target_number:
        print(f"Target: {target_number}")
        result = total - target_number
        if result>= 0:
            print(f"WIN  {result}")
        else:
            print(f"FAIL {result}")

    return total


def main():
    import sys
    if len(sys.argv) < 2:
        print("Usage: rolldice.py <dice_code> [target_number]")
        print("Examples: \n  rolldice.py 2d6+3\n  rolldice.py \"1d6 +2 +2d10 -4\" 17")
    else:
        dice_code = sys.argv[1]
        target_number = None
        if len(sys.argv) == 3:
            target_number = int(sys.argv[2])
        roll_dice(dice_code, target_number)

if __name__ == "__main__":
    main()

