#!/usr/bin/env python3
import sys
import random
from enum import Enum
import math

__all__ = ["ndx","explode", "eotw", "risk"]

# probability functions
C = lambda n,m: math.factorial(n)/(math.factorial(m)*math.factorial(n-m))
binprob = lambda n,m,p: C(n,m) * p**m * (1-p)**(n-m)
P = lambda desired,possible: desired/possible


# roll n x-sided dice
def ndx(n,x,o=0):
    pool = []
    start = 1+o
    stop = x+o
    for i in range(n):
        pool.append(random.randint(start,stop))
    return pool


# roll n fate dice
def ndf(n):
    return ndx(n,3,-2)


# roll n d10 with [0-9]
def nd09(n):
    return ndx(n,10,-1)


# roll n d%
def ndp(n):
    return ndx(n,100,-1)


# do n coin flips (i.e. d2 with [0,1])
def coin(n,o=0):
    return ndx(n,2,-1+o)


# explode rolls of x
def explode(pool, x):
    for roll in pool:
        if roll == x:
            pool.append(random.randint(1,x))
    return pool


# roll p positive, n negative dice, compare to target t
# X: remove negative dice <= success dice
def eotw(p,n,t,X=False):
    pos = sorted(ndx(p,6), reverse=True)
    neg = sorted(ndx(n,6), reverse=True)
    cancel = []
    failed = []
    removed = []

    for roll in pos[:]:
        if roll in neg:
            pos.remove(roll)
            neg.remove(roll)
            cancel.append(roll)
        elif roll > t:
            pos.remove(roll)
            failed.append(roll)
    if X:
        for (rp, rn) in zip(pos,neg):
            if rp > rn:
                neg.remove(rn)
                removed.append(rn)
 
    return (len(pos), len(neg), pos, neg, cancel, failed, removed)


# risk style. roll attacker and defender, compare rolls, higher wins
def risk(a,d):
    attack = sorted(ndx(a,6), reverse=True)
    defend = sorted(ndx(d,6), reverse=True)

    l = d if d <= a else a
    attack = (attack[:l], attack[l:])
    defend = (defend[:l], defend[l:])
    for (ra,rd) in zip(attack[0][:],defend[0][:]):
        if rd >= ra:
            attack[0].remove(ra)
            attack[1].append(ra)
        else:
            defend[0].remove(rd)
            defend[1].append(rd)

    return (len(attack[0])-l, len(defend[0])-l, attack, defend)


# success functions
even = lambda x: x%2 == 0
odd = lambda x: x%2 == 1
ge4 = lambda x: x>=4
ge5 = lambda x: x>=5
yz = lambda x: x==6


# creates a success function with minimum target number
# t: target number to roll higher or equal
def minroll_factory(t):
    return lambda x: x>=t


# creates a success function with maximum target number
# t: target number to roll lower or equal
def maxroll_factory(t):
    return lambda x: x<=t


# count successes by comparing rolls to function fn
def successes(pool,fn):
    sucs = []
    for roll in pool:
        if fn(roll):
            sucs.append(roll)
    return (len(sucs), sucs) 


def main():
    n = random.randint(1,10)
    x = random.randint(4,20)
    t = random.randint(1,6)

    pool = ndx(n,x)
    print("pool: %s" %pool)

    pool = explode(pool,x)
    print("exploded: %s" %pool)

    fn = minroll_factory(t)
    (nsucs,sucs) = successes(pool,fn)
    print("successes: %s %s" %(nsucs,sucs))

    print("\nThe End of the World")
    p = random.randint(1,4)
    n = random.randint(1,4)
    test = eotw(p,n,t) 
    print(test)

    print("\nHomebrew EotW)")
    test = eotw(p,n,t,True) 
    print(test)

    print("\nRISK")
    a = random.randint(1,5)
    b = random.randint(1,5)
    test = risk(a,b)
    print(test)



if __name__ == '__main__':
    main()

