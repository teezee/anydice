#!/bin/env python

import sys
from random import shuffle

def sort(pack: []):
    pack.sort()


def draw(pack: [], num_cards: int = 1, hand: [] = []):
    #print(f"draw pack {pack} num {num_cards} hand {hand}")
    hand.extend([pack.pop() for _ in range(num_cards)])
    return hand


def discard(pack: [], num_cards: int = 1, discarded: [] = []):
    #print(f"discard pack {pack} num {num_cards} discarded {discarded}")
    #discarded.extend([pack.pop() for _ in range(num_cards)])
    #return discarded
    return draw(pack, num_cards, discarded)


class Card:
    ACE      = 1
    JACK     = 11
    QUEEN    = 12
    KING     = 13
    SPADES   = 0
    HEARTS   = 1<<4
    DIAMONDS = 2<<4
    CLUBS    = 3<<4
    RANK     = 0b00_1111  # Rank bitmask
    SUIT     = 0b11_0000  # Suit bitmask

    def __init__(self,rank,suit):
        self.face = rank | suit


    def __str__(self):
        # Unicode cards start at U+1F0A1. Suit order and the
        # gap of 16 between suits is the same as in this code
        code_point = 0x1F0A0 + self.face
        after_c = self.face & Card.RANK > Card.JACK  # Skip Knight cards 🂬 , 🂼 , 🃌 , 🃜
        if after_c:
            code_point += 1

        return chr(code_point)


    def __repr__(self):
        return self.__str__()


    def __lt__(self, other):
         return self.face < other.face

    def __gt__(self, other):
         return self.face > other.face

    def __eq__(self, other):
         return self.face == other.face


    @staticmethod
    def pack(skat: bool = False):
        range_suit = range(Card.SPADES, Card.CLUBS+1, 16)
        if skat:
            # skat pack of 32 cards
            range_rank = [1,7,8,9,10,11,12,13]
        else:
            # full pack of 52 cards
            range_rank = range(Card.ACE, Card.KING+1)
        
        return [Card(rank, suit) for suit in range_suit for rank in range_rank]


def main(argv):
    skat = False
    if len(argv) == 1:
        skat = True
        print("Using Skat pack")
    deck = Card.pack(skat)
    shuffle(deck)
    hand = []
    discarded = []
    while True:
        cmd = input("command: ")
        cmd = cmd.lower()
        if cmd in ['q', 'quit', 'e', 'exit']:
            break
        elif 'dr' in cmd:
            num = int(cmd.split(' ')[1])
            print(f"draw {num} cards")
            draw(deck, num, hand)
        elif 'dis' in cmd:
            num = int(cmd.split(' ')[1])
            print(f"discard {num} cards")
            discard(hand, num, discarded)
        elif 'ha' in cmd:
            print(hand)
        else:
            print("unknown command")


if __name__ == '__main__':
    main(sys.argv[1:])

