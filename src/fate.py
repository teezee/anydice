#!/usr/bin/env python3
import sys
from enum import Enum
from random import randint
#from anydice import ndf


def dF(n: int = 4) -> []:
    pool = [randint(-1,1) for _ in range(n)]
    return pool


class Fate:
    class Ladder(Enum):
        legendary=8
        epic = 7
        fantastic = 6
        superb = 5
        great = 4
        good = 3
        fair = 2
        average = 1
        mediocre = 0
        poor = -1
        terrible = -2
        catastrophic = -3
        horrifying = -4

        @classmethod
        def _missing_(cls,value):
            if isinstance(value,int):
                if value < -4:
                    value = -4
                elif value > 8:
                    value = 8
                return cls(value)
            return super()._missing_(value)

        def __repr__(self):
            return "%s (%s)" %(self.name, self.value)


    @staticmethod
    def roll(m, d) -> ():
        if isinstance(m, Fate.Ladder):
            m = m.value
        if isinstance(d, Fate.Ladder):
            d = d.value
        
        #pool = ndf(4)
        pool = dF()
        result = sum(pool) + m - d
        ladder = Fate.Ladder(result)
        return (ladder, "= %s + %s -%s" %(pool, m, d))


def main():
    if len(sys.argv) < 3:
        print("usage: %s <modifier> <difficulty>" %sys.argv[0])
        sys.exit(1)

    try:
        m = int(sys.argv[1])
    except:
        try:
            m = Fate.Ladder[sys.argv[1]]
        except Exception as e:
            raise e
    try:
        d = int(sys.argv[2])
    except:
        try:
            d = Fate.Ladder[sys.argv[2]]
        except Exception as e:
            raise e

    test = Fate.roll(m,d)
    print(test)


if __name__ == '__main__':
    main()

