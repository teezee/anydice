#!/usr/bin/env python3
import sys
import random
import logging
from anydice import eotw
from telegram import Update, ParseMode
from telegram.ext import Updater, CommandHandler, CallbackContext


def help(update: Update, context: CallbackContext):
    response = ("*The End of the World roll bot*\n"
            "To make a test for The End of the World RPG use:\n"
            "\t/eotw P N T\n "
            "where \n"
            "P: number of positive dice \n"
            "N: number of negative dice \n"
            "T: the target number to beat \n"
            "\n"
            "License: Do the Fuck what you want"
    )
    logging.info("response")
    context.bot.send_message(chat_id=update.message.chat_id, text=response, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)


def eotw_handler(update: Update, context: CallbackContext):
    username = update.message.from_user.username if update.message.from_user.username else update.message.from_user.first_name
    try:
        p = int(context.args[0])
        n = int(context.args[1])
        t = int(context.args[2])
        (success, stress, pos, neg, canceled, failed, removed) = eotw(p,n,t,True)
        response = (f'@{username} rolled:\n successes: {str(success)}  [{str(pos)}, {str(failed)}]\n stress: {str(stress)}  [{str(neg)}, {str(removed)}]\n cancelled: [{str(canceled)}]\n')
    except Exception as e:
        response = f'@{username}: *Exception*: {str(e)}. Use "/eotw P N T"\n'
        logging.error(e)
    
    context.bot.send_message(chat_id=update.message.chat_id, text=response, parse_mode=ParseMode.MARKDOWN)


def main():
    if len(sys.argv) < 2:
        print("usage: %s <API_KEY>" %sys.argv[0])
        sys.exit(1)

    API_KEY = str(sys.argv[1])

    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

    updater = Updater(token=API_KEY, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('eotw', eotw_handler))
    updater.start_polling()


if __name__ == '__main__':
    main()

