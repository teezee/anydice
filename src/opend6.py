#!/usr/bin/env python3
import sys
import random
from enum import Enum


def d6(n: int = 1) -> []:
    pool = [ random.randint(1,6) for _ in range(n) ] 
    return pool


# explode rolls of x
def explode(pool, x):
    for roll in pool:
        if roll == x:
            pool.append(random.randint(1,x))
    return pool


class fstr:
    REDBG = '\033[41m'
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    ULINE = '\033[4m'
    BOLD = '\033[1m'
    END = '\033[0m'


class Opend6:
    class Dicecode:
        @classmethod
        def split(cls, code: str) -> ():
            n, pips = code.upper().split("D")
            if pips == '':
                pips = "0"
            return (int(n), int(pips))

        @classmethod
        def cat(cls, n: int, pips: int) -> str:
            return "%sD+%s" %(n,pips)

        @classmethod
        def from_pips(cls, pips: int) -> str:
            return str(int(pips/3)) + "D" + "+" + str(pips%3)

        @classmethod
        def to_pips(cls, code: str) -> int:
            dice,pips = cls.split(code)
            return int(dice)*3 + int(pips)

        @classmethod
        def split_pips(cls, pips: int) -> ():
            code = cls.from_pips(pips)
            return cls.split(code)

        @classmethod
        def to_mean(cls, n: int, pips: int) -> int:
            return n * (6+1)/2 + pips

        @classmethod
        def from_mean(cls, mean: int) -> ():
            return (int(mean*2/(6+1)), int(mean%((6+1)/2)%3))
    

    class Difficulty(Enum):
        # description, min, max, legends
        TRIVIAL=(1,5,1)
        EASY=(6,10,2)
        MODERATE=(11,15,3)
        DIFFICULT=(16,20,4)
        HARD=(21,25,5)
        HEROIC=(26,30,6)
        LEGENDARY=(31,35,7)
        IMPOSSIBLE=(36,40,8)

        def roll(self):
            target = ( random.randint(self.value[0],self.value[1]), self.value[2] )
            return target

        @classmethod
        def random(cls):
            return random.choice(list(cls))

        @classmethod
        def target(cls, arg: str) -> ():
            roll, target_o, target_l, code = (None, None, None, None)
            try:
                target_o, target_l = (int(arg), int(arg))
            except:
                try:
                    target_o, target_l = Opend6.Difficulty[str(arg.upper())].roll()
                except:
                    try:
                        roll, target_o, target_l, code = Opend6.roll(arg)
                    except Exception as e:
                        print(f"Error setting difficulty '{arg}'", file=sys.stderr)
                        #raise e 
            return (roll, target_o, target_l, code) 


    #def roll(code: str, n: int, pips: int):
    @staticmethod
    def roll(code: str):
        n, pips = Opend6.Dicecode.split(code)
        pool = d6(n)
        # last dice is wild
        wild = pool[-1:]
        pool = pool[:-1]
        pool.extend(explode(wild,6))
        num_open = sum(pool)+pips
        num_successes = sum(1 for x in pool if x >=3)
        #return (pool, num_open, num_successes, Opend6.Dicecode.cat(n,pips))
        return (pool, num_open, num_successes, code)


    @staticmethod
    def roll_to_str(roll_l: list) -> str:
        pool = roll_l[0]
        # as unicode dice
        #pool = [ str(f'{chr(0x2680 | int(d-1))}') for d in roll_l[0] ]
        n, pips = Opend6.Dicecode.split(roll_l[3])
        wild_i = n-1 
        # bold & underline wild die
        if len(pool) == 1:
            pool_s = "[" + f"{fstr.ULINE}{fstr.BOLD}{pool[wild_i]}{fstr.END}]"
        elif len(pool) == n:
            pool_s = "[" + str(pool[:wild_i])[1:-1] + f", {fstr.ULINE}{fstr.BOLD}{pool[wild_i]}{fstr.END}]"
        else:
            pool_s = "[" + str(pool[:wild_i])[1:-1] + f", {fstr.ULINE}{fstr.BOLD}{pool[wild_i]}{fstr.END}, " + str(pool[wild_i+1:])[1:-1] + "]"
        roll_s = f"rolled {roll_l[3]}: {pool_s} sum: {roll_l[1]} successes: {roll_l[2]}"
        return roll_s


    #def test(n: int, pips: int, difficulty: str) -> ():
    @staticmethod
    def test(roll: (), target_open: int, target_legend: int) -> ():
        #target_open, target_legend = Opend6.Difficulty.target(difficulty)
        n, pips = Opend6.Dicecode.split(roll[3])
        wild = roll[0][n-1]
        result_open = roll[1] - target_open
        result_legend = roll[2] - target_legend 

        s_success = f"{fstr.GREEN}Success{fstr.END}"
        s_fail = f"{fstr.RED}Fail{fstr.END}"
        s_mishap = f"{fstr.YELLOW}Success w/ mishap{fstr.END}"
        s_crit = f"{fstr.REDBG}Fail Critical{fstr.END}"

        if wild == 1:
            if result_open >= 0:
                result_open_s = s_mishap
            else:
                result_open_s = s_crit
            if result_legend >= 0:
                result_legend_s = s_mishap
            else:
                result_legend_s = s_crit
        else:
            if result_open >= 0:
                result_open_s = s_success
            else:
                result_open_s = s_fail 
            if result_legend >= 0:
                result_legend_s = s_success
            else:
                result_legend_s = s_fail

        result_o = (result_open, result_open_s, target_open)
        result_l = (result_legend, result_legend_s, target_legend)
        return (result_o, result_l)


    @staticmethod
    def result_str(result_l: list, legend: bool = False) -> str:
        pfx = "OpenD6:"
        if legend:
            pfx = "D6Legend:"
        num, phrase, target = result_l
        #result_s = f"{pfx} target {target} result {num} -> {phrase.upper()}"
        result_s = f"{pfx} target {target} result {num} -> {phrase}"
        return result_s


def roll(argv: []):
    code = argv[0]
    roll = Opend6.roll(code)
    print(f"You {Opend6.roll_to_str(roll)}")

    if len(argv) == 2:
        difficulty = Opend6.Difficulty.target(argv[1])
        if difficulty[0]:
            print(f"Target {Opend6.roll_to_str(difficulty)}")
        if difficulty[1]!=None and difficulty[2]!=None:
            result_o, result_l = Opend6.test(roll, difficulty[1], difficulty[2])
            print(Opend6.result_str(result_o))
            print(Opend6.result_str(result_l, True))


def usage():
    print("usage: %s <dicecode> [difficulty]" %sys.argv[0])
    print("  <dicecode>:   a D6 system dice code, e.g. '3d+2', '2d', '4d+1'")
    print("  [difficulty]: number, dicecode, or")
    print("                trivial, easy, moderate, difficult, hard, heroic, legendary, impossible")


def main():
    if len(sys.argv) < 2:
        usage()
        #sys.exit(1)

    if len(sys.argv) > 1:
        roll(sys.argv[1:])
    else:
        while True:
            print()
            cmd = input("roll> ")
            if cmd.lower() in ['q','e', 'quit', 'exit']:
                break
            roll(cmd.split(' '))


if __name__ == '__main__':
    main()

